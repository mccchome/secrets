// The analyzer program finds leaks of secrets like token and cryptographic keys in a directory.
package main

import (
	"os"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/security-products/analyzers/command/v2"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v3/logutil"
	"gitlab.com/gitlab-org/security-products/analyzers/secrets/v3/metadata"
	"gitlab.com/gitlab-org/security-products/analyzers/secrets/v3/plugin"
)

func main() {
	app := cli.NewApp()
	app.Name = "analyzer"
	app.Version = metadata.AnalyzerVersion
	app.Authors = []*cli.Author{{Name: metadata.AnalyzerVendor}}
	app.Usage = metadata.AnalyzerUsage

	log.SetFormatter(&logutil.Formatter{Project: metadata.AnalyzerName})
	log.Info(metadata.AnalyzerUsage)

	app.Commands = command.NewCommands(command.Config{
		Match:        plugin.Match,
		Analyze:      analyze,
		Analyzer:     metadata.AnalyzerDetails,
		AnalyzeFlags: analyzeFlags(),
		AnalyzeAll:   true,
		Convert:      convert,
		Scanner:      metadata.ReportScanner,
		ScanType:     metadata.Type,
		ArtifactName: command.ArtifactNameSecretDetection,
	})

	// This is a hack until we add Secret Detection Excluded Paths override in the command package
	app.Commands[0] = overrideExcludedPaths(app.Commands[0])

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}

// overrideExcludedPaths replaces the default `--excluded-paths` (env var SAST_EXCLUDED_PATHS) with
// a new secret detection excluded flag `SECRET_DETECTION_EXCLUDED_PATHS`
// TODO https://gitlab.com/gitlab-org/gitlab/-/issues/320967 removes the need for this function.
func overrideExcludedPaths(c *cli.Command) *cli.Command {
	flags := []cli.Flag{}
	for _, f := range c.Flags {
		if f.Names()[0] == flagExcludedPaths {
			continue
		}
		flags = append(flags, f)
	}
	c.Flags = append(flags, &cli.StringSliceFlag{
		Name:    flagExcludedPaths,
		EnvVars: []string{"SECRET_DETECTION_EXCLUDED_PATHS"},
		Usage:   "Comma-separated list of paths (globs supported) to be excluded from the output.",
	})
	return c
}
